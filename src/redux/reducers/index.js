import { combineReducers } from "redux"
import { connectRouter } from "connected-react-router"
import history from "../..//utils/history"
// import { allProductList } from "./allProductList"

const appReducer = combineReducers({
  router: connectRouter(history),
})

const rootReducer = (state, action) => {
  if (action && action.type === "SIGN_OUT") {
    state = undefined
  }
  return appReducer(state, action)
}

export default rootReducer
