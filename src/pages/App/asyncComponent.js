import React from "react"
import Layout from "../../components/Layout"

const asyncComponent = (importString) => {
  return React.lazy(() => import(`../${importString}`))
}

const AsyncComponents = [
  Layout,
  Layout,
  [
    [asyncComponent("Home"), ["home"], "private"],
    [asyncComponent("Authentation"), ["/"], "public"],



  ],
]

export default AsyncComponents
