import { loadState } from "../utils/storageUtil"
import { OKHATI } from "./constants/appConfigs"

export let isAllowed = (permissionCode) => {
  const permissions = [loadState(OKHATI)?.status]
  let isAuthorized = false
  Array.isArray(permissionCode) &&
    permissionCode.forEach((code) => {
      if (Array.isArray(permissions) && permissions.includes(code)) {
        isAuthorized = true
      }
    })
  return isAuthorized
}
