import moment from "moment"
import { loadState, removeState } from "./storageUtil"
import { OKHATI } from "./constants/appConfigs"

export let isTokenExpired = () => {
  const expiresIn = loadState(OKHATI)
    ?.email?.[`.expires`]?.split(",")
    .join(" ")
  if (moment(expiresIn).valueOf() > Date.now()) {
    // Checking if token is expired.
    return true
  } else {
    removeState(OKHATI)
    return false
  }
}

export let isAuthenticated = () => {
  return loadState(OKHATI)?.status
}
