import { ConnectedRouter } from "connected-react-router"
import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { Router } from "react-router-dom"
import App from "./pages/App/index"
import reportWebVitals from "./reportWebVitals"
import configureStore from "./redux/store/configureStore"
import history from "./utils/history"
import '../src/sass/App.scss'
const mountNode = document.getElementById("root")
const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Router  history={history}>
          <App />
      </Router>
    </ConnectedRouter>
  </Provider>,
  mountNode
)

reportWebVitals()
