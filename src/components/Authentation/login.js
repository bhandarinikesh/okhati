import { Button} from '@mui/material';
import React from 'react'
import { useForm,FormProvider} from "react-hook-form";
import { OKHATI } from '../../utils/constants/appConfigs';
import history from '../../utils/history';
import { saveState } from '../../utils/storageUtil';
import {CommonInput,Pawwsord} from '../Common';
const Login=(props)=>{
    const {onTime}=props
    const methods = useForm();
    const onSubmit = (data) =>{
        saveState(OKHATI,{...data,message:'Login successfully ',success:'success',error:'success',status:1})
        onTime(true)
        history.push('/app/home')
    };

    return(
            <FormProvider {...methods} > 
            <form onSubmit={methods.handleSubmit(onSubmit)}>
                <div className='inputtype'>
                <CommonInput 
                fullWidth
                label="Email"
                variant="filled"
                type="email"
                name="email"
                required
                id="outlined-required"
               />
                </div>
              <div className='inputtype'>
              <Pawwsord 
              fullWidth
                id="outlined-required"
                label="Password"
                variant="filled"
                name="password"
                type="password"
                required
               />
              </div>
              <div className='inputtype'>
              <Button type="submit" variant="contained" color="primary">
                Login
                </Button>
              </div>
              
            </form>
            </FormProvider > 
    )
}
export default Login