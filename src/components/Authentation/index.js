import { Container } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { OKHATI } from '../../utils/constants/appConfigs'
import { loadState } from '../../utils/storageUtil'
import { DescriptionAlerts } from '../Common'
import Login from './login'
import Register from './register'
const Index=()=>{
  
  const [iActive,setIsActive]=useState(1)
  const [isTime,setTime]=useState()
  const authdata=loadState(OKHATI)
  const handelSet=(e,id)=>{
    setIsActive(id)
  }
  useEffect(()=>{
     setTimeout(() => {
        setTime(false)
     }, 3000);
  },[authdata,iActive])
    return(
        <div>
     
        <Container maxWidth="sm" className='auth-content'>
        <div className='auth'>
            {
                data.map((item)=>(
                    <div key={item.id} onClick={(e)=>handelSet(e,item.id)} >
                        <div className={iActive === item.id ? 'register isactive':"register inactive"}>
                        {item.name}

                        </div>
                    </div>
                ))
            }
         
        </div>

               {
             iActive ===2 ? (
                <Login onTime={(e)=>setTime(e)}/>
             ):(
                <Register onRegister={(e)=>setIsActive(e)} iActive={iActive} onTime={(e)=>setTime(e)}/>
             )
            }
                {
                isTime &&(
            <div style={{position:'fixed',top:'5rem',right:'8rem'}}>
            <DescriptionAlerts severity={authdata?.success} message={authdata?.message} error={authdata?.error}/>

            </div>
            )
            }
        </Container>
        </div>
    )
}
export default Index
const data=[
    {
        id:1,
        name:'Sign Up'
    },
    {
        id:2,
        name:'Login'

    }
   
]