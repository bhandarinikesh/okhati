import { Button } from '@mui/material';
import React from 'react'
import { useForm,FormProvider} from "react-hook-form";
import { OKHATI } from '../../utils/constants/appConfigs';
import { saveState } from '../../utils/storageUtil';
import {CommonInput,Pawwsord} from '../Common';
const Register=(props)=>{
    const {onRegister,onTime}=props
    const methods = useForm();
    const onSubmit = (data) =>{
        saveState(OKHATI,{...data,message:'Register successfully ',success:'success',error:'success'})
        onTime(true)
        onRegister(2)
    }
    return(
        <div>
            <FormProvider {...methods} > 
            <form onSubmit={methods.handleSubmit(onSubmit)}>
                <div className='inputtype'>
                <CommonInput 
                label="Email"
                variant="filled"
                type="email"
                name="email"
                fullWidth
                required
                id="outlined-required"

               />
                </div>
               <div className='inputtype'>
               <Pawwsord 
                 fullWidth
                id="outlined-required"
                label="Password"
                variant="filled"
                name="password"
                type="password"
                required
                min={8}
                validate

               />
               </div>
            
                <div className='inputtype'>
                <Pawwsord 
                 fullWidth
                id="outlined-required"
                label="Conform Password"
                variant="filled"
                name="conform_password"
                type="password"
                required
              
               />
                </div>
               <div className='inputtype'>
               <Button type="submit" variant="contained" color="primary">
                 Sign Up
                </Button>
               </div>
                
            </form>
            </FormProvider > 
       
          

        </div>
    )
}
export default Register