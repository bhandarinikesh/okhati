import { Alert, AlertTitle, CircularProgress, TextField } from "@mui/material";
import { Controller,useFormContext } from "react-hook-form";

export const  CommonInput = (props)=> {
    const {
      name ,
      label,
      type,
      size ,
      variant ,
      placeholder ,
      fullWidth ,
      className ,
      disabled ,
      multiline,
      rows, 
      maxRows,
      maxLength ,
      defaultValue,
      style,
    } = props;
    const {control, register } = useFormContext() 
   
    
    return (
      <Controller
        control={control}
        defaultValue={defaultValue}
        name={name}
        render={({ field: { onChange, value }, fieldState: { error } }) => {
          return (
            <TextField
              {...register(name)}
              style={style}
              className={className}
              maxLength={maxLength}
              value={value}
              label={label}
              onChange={onChange}
              error={!!error}
              helperText={error ? error.message : null}
              disabled={disabled}
              fullWidth={fullWidth}
              variant={variant}
              size={size}
              multiline={multiline}
              rows={rows || 1}
              maxRows={maxRows}
              type={type}
              placeholder={placeholder}
              key={name}
            />
          );
        }}
        
        rules={{ required:`Plese input your ${name}`}}
      />
    );
  }
  
  export const  Pawwsord = (props)=> {
    const {
      name ,
      label,
      type,
      size ,
      variant ,
      placeholder ,
      fullWidth ,
      className ,
      disabled ,
      multiline,
      rows, 
      maxRows,
      maxLength ,
      defaultValue,
      style,
    } = props;
    const {control, register ,getValues} = useFormContext() 
   
    
    return (
      <Controller
        control={control}
        defaultValue={defaultValue}
        name={name}
        render={({ field: { onChange, value }, fieldState: { error } }) => {
        
          return (
            <TextField
              {...register(name)}
              style={style}
              className={className}
              maxLength={maxLength}
              value={value}
              label={label}
              onChange={onChange}
              error={!!error}
              helperText={error ? error.message : null}
              disabled={disabled}
              fullWidth={fullWidth}
              variant={variant}
              size={size}
              multiline={multiline}
              rows={rows || 1}
              maxRows={maxRows}
              type={type}
              placeholder={placeholder}
              key={name}
            />
          );
        }}
        
        rules={{ validate: (value) => {

          if(name === "conform_password"){
            const { password } = getValues()
            return password === value || "Passwords should match!";
          }else{
            return (
              [/[a-z]/, /[A-Z]/, /[0-9]/, /[^a-zA-Z0-9]/].every((pattern) =>
                pattern.test(value)
              ) || "must include lower, upper, number, and special chars"
            );
          }
          
          
        },
        minLength: {
          value: 8,
          message: "must be 8 chars",
        },
        required: name,
        }}
      />
    );
  }
  export const  DescriptionAlerts=(props)=> {
    const {
      severity,
      error,
      message
    }=props
  
    return (
      <div >
        <Alert severity={severity}>
          <AlertTitle>{error}</AlertTitle>
          {message}
        </Alert>
      </div>
    );
  }
  
export const Loader=()=>{
  return(
      <div>
           <CircularProgress color="success" />
      </div>
  )
}