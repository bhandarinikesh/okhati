import { Button } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { OKHATI } from '../../utils/constants/appConfigs'
import history from '../../utils/history'
import { loadState, removeState } from '../../utils/storageUtil'
import { DescriptionAlerts } from '../Common'
const Home=()=>{
  const [isTime,setTime]=useState(true)
  const authdata=loadState(OKHATI)
   
    useEffect(()=>{
        setTimeout(() => {
           setTime(false)
        }, 3000);
     },[])
    return(
        <div className='home-content'>
                <p>Welcome to okhati</p>
                <Button onClick={()=>{ removeState(OKHATI)
                  history.push(`/`)}}
                  >
                    Logout
                </Button>
                {
                isTime &&(
            <div style={{position:'fixed',top:'5rem',right:'8rem'}}>
            <DescriptionAlerts severity={authdata?.success} message={authdata?.message} error={authdata?.error}/>

            </div>
            )
            }
        </div>
    )
}
export default Home