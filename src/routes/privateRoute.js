import React, { Suspense } from "react"
import { Route, Redirect } from "react-router-dom"
import { isAuthenticated } from "../utils/authUtil"
import {Loader} from "../components/Common"

const PrivateRoute = ({ component: Component, layout: Layout, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isAuthenticated()) {
          return (
            <Redirect
              to={{
                pathname: "/",
                state: { from: props.location },
              }}
            />
          )
        }
        return (
          <Layout>
            <Suspense fallback={<Loader />}>
              <Component {...props} />
            </Suspense>
          </Layout>
        )
      }}
    />
  )
}

export default PrivateRoute
