import React, { Suspense } from "react"
import { Route,Redirect } from "react-router-dom"
import {Loader} from "../components/Common"
import { isAuthenticated } from "../utils/authUtil"

const PublicRoute = ({ component: Component, layout: Layout, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (isAuthenticated()) {
          return (
            <Redirect
              to={{
                pathname: "/app/home",
                state: { from: props.location },
              }}
            />
          )
        }
        // authorised so return component
        return (
          <Layout>
            <Suspense fallback={<Loader />}>
              <Component {...props} />
            </Suspense>
          </Layout>
        )
      }}
    />
  )
}

export default PublicRoute
